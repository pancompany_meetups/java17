package com.pancompany.java17.enhanced;

import com.pancompany.java17.record.Coffee;
import com.pancompany.java17.record.Size;

public class Barista {

    // Switch expression
    public static Coffee percolate(final Size size) {
        final var coffee =  switch (size) {
            case SMALL -> new Coffee(size, 1.0);
            case MEDIUM -> new Coffee(size, 1.76);
            case LARGE, VENTI -> new Coffee(size, 2.3);
        };

        return coffee;
    }

    // Switch statement 'new'
    public static Coffee percolateSwitchStatement(final Size size) {
        final Coffee coffee;
        switch (size) {
            case SMALL -> {
                coffee = new Coffee(size, 1.0);
            }
            case MEDIUM -> {
                coffee =  new Coffee(size, 1.76);
            }
            case LARGE, VENTI -> {
                coffee = new Coffee(size, 2.3);
            }
            default -> throw new IllegalStateException("Don't know the price for this size");
        }

        return coffee;
    }

    // Switch statement
    public static Coffee percolateTheOldWay(final Size size) {
        final Coffee coffee;
        switch (size) {
            case SMALL:
                coffee = new Coffee(size, 1.0);
                break;
            case MEDIUM:
                coffee =  new Coffee(size, 1.76);
                break;
            case LARGE, VENTI:
                coffee =  new Coffee(size, 2.3);
                break;
            default:
                throw new IllegalStateException("Don't know the price for this size");
        }
        return coffee;
    }


}
