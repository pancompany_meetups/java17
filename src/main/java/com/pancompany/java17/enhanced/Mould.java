package com.pancompany.java17.enhanced;

import com.pancompany.java17.pattern.Round;
import com.pancompany.java17.pattern.Shape;
import com.pancompany.java17.pattern.Square;

public class Mould {

    public static void form(final Shape shape) {
        shape.cast();

        if (shape instanceof Round round) {
            round.stretch();
        } else if (shape instanceof Square square) {
            square.level();
            // Do some more stuff
        } else {
            throw new IllegalStateException("Not a known shape");
        }
    }

    public static void formTheOldWay(final Shape shape) {
        shape.cast();

        if (shape instanceof Round) {
            ((Round) shape).stretch();
        } else if (shape instanceof Square) {
            final Square square = (Square) shape;
            square.level();
            // Do some more stuff
        } else {
            throw new IllegalStateException("Not a known shape");
        }
    }

}
