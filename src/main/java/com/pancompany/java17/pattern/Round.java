package com.pancompany.java17.pattern;

public final class Round implements Shape {

    private boolean formed;

    public void stretch(){
        System.out.println("Stretching round");
    }

    @Override
    public boolean isFormed() {
        return formed;
    }

    @Override
    public void cast() {
        System.out.println("Casting round");
        formed = true;
    }

    @Override
    public int length() {
        return 10;
    }
}
