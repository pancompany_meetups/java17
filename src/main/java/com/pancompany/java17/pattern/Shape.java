package com.pancompany.java17.pattern;

public sealed interface Shape permits Round, Square {

    boolean isFormed();

    void cast();

    int length();
}
