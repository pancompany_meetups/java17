package com.pancompany.java17.pattern;

public final class Square implements Shape {

    private boolean formed;

    public void level(){
        System.out.println("Leveling the square's lines");
    }

    @Override
    public boolean isFormed() {
        return formed;
    }

    @Override
    public void cast() {
        System.out.println("Casting square");
        formed = true;
    }

    @Override
    public int length() {
        return 20;
    }
}
