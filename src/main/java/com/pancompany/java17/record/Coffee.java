package com.pancompany.java17.record;

public record Coffee(Size size, double price) {

    public Coffee {
        if(price < 0){
            throw new IllegalArgumentException("Price should not be negative");
        }
    }
}
