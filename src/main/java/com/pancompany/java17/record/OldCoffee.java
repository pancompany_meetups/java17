package com.pancompany.java17.record;

import java.util.Objects;

public final class OldCoffee {

    private final Size size;
    private final double price;

    public OldCoffee(final Size size, final double price) {
        if(price < 0){
            throw new IllegalArgumentException("Price should not be negative");

        }

        this.size = size;
        this.price = price;
    }

    public Size getSize() {
        return size;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "OldCoffee{" +
                "size=" + size +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final OldCoffee oldCoffee = (OldCoffee) o;
        return Double.compare(oldCoffee.price, price) == 0 && size == oldCoffee.size;
    }

    @Override
    public int hashCode() {
        return Objects.hash(size, price);
    }
}
