package com.pancompany.java17.record;

public enum Size {
    SMALL,
    MEDIUM,
    LARGE,
    VENTI // Venti is Twenty...
}
