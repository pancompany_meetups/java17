package com.pancompany.java17.enhanced;

import com.pancompany.java17.record.Coffee;
import com.pancompany.java17.record.Size;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static org.junit.jupiter.api.Assertions.*;

class BaristaTest {

    @Nested
    class Percolate {

        @EnumSource(value = Size.class)
        @ParameterizedTest(name = "Showcase for switch expression with a Coffee of size {0}")
        void size(final Size size) {
            final Coffee coffee = Barista.percolate(size);

            assertEquals(size, coffee.size());
        }

        @EnumSource(value = Size.class)
        @ParameterizedTest(name = "Showcase for switch expression with a Coffee of size {0}")
        void price(final Size size) {
            final Coffee coffee = Barista.percolate(size);

            assertNotNull(coffee.price(), "Never null ^^");
        }
    }

    @Nested
    class PercolateSwitchStatement {

        @EnumSource(value = Size.class)
        @ParameterizedTest(name = "Showcase for switch expression with a Coffee of size {0}")
        void size(final Size size) {
            final Coffee coffee = Barista.percolateSwitchStatement(size);

            assertEquals(size, coffee.size());
        }

        @EnumSource(value = Size.class)
        @ParameterizedTest(name = "Showcase for switch expression with a Coffee of size {0}")
        void price(final Size size) {
            final Coffee coffee = Barista.percolateSwitchStatement(size);

            assertNotNull(coffee.price(), "Never null ^^");
        }
    }

    @Nested
    class PercolateTheOldWay {

        @EnumSource(value = Size.class)
        @ParameterizedTest(name = "Showcase for switch statement with a Coffee of size {0}")
        void size(final Size size) {
            final Coffee coffee = Barista.percolateTheOldWay(size);

            assertEquals(size, coffee.size());
        }

        @EnumSource(value = Size.class)
        @ParameterizedTest(name = "Showcase for switch statement with a Coffee of size {0}")
        void price(final Size size) {
            final Coffee coffee = Barista.percolateTheOldWay(size);

            assertNotNull(coffee.price(), "Never null ^^");
        }
    }

}