package com.pancompany.java17.enhanced;

import com.pancompany.java17.pattern.Round;
import com.pancompany.java17.pattern.Square;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MouldTest {

    @Test
    @DisplayName("Showcase for sealed classes (round shape)")
    void round(){
        final Round round = new Round();
        assertFalse(round.isFormed());

        Mould.form(round);

        assertTrue(round.isFormed());
    }

    @Test
    @DisplayName("Showcase for sealed classes (square shape)")
    void square(){
        final Square square = new Square();
        assertFalse(square.isFormed());

        Mould.form(square);

        assertTrue(square.isFormed());
    }

//    @Test
//    void unknown(){
//        final Shape shape = new Shape(){
//            @Override
//            public void cast() {
//
//            }
//        }
//    }

}