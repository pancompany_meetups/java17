package com.pancompany.java17.record;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class OldCoffeeTest {

    @Nested
    class NotRecord {

        @EnumSource(value = Size.class)
        @ParameterizedTest(name = "Showcase for record (size) with a OldCoffee of size {0}")
        void size(final Size size) {

            final OldCoffee toBeMade = new OldCoffee(size, 0.0);
            final OldCoffee expected = new OldCoffee(size, 0.0);

            assertEquals(expected.getSize(), toBeMade.getSize());
        }

        @EnumSource(value = Size.class)
        @ParameterizedTest(name = "Showcase for record (price) with a OldCoffee of size {0}")
        void price(final Size size) {

            final OldCoffee toBeMade = new OldCoffee(size, 0.0);
            final OldCoffee expected = new OldCoffee(size, 0.0);

            assertEquals(expected.getPrice(), toBeMade.getPrice());
        }

        @EnumSource(value = Size.class)
        @ParameterizedTest(name = "Showcase for record (equals) with a OldCoffee of size {0}")
        void areEqual(final Size size) {

            final OldCoffee toBeMade = new OldCoffee(size, 0.0);
            final OldCoffee expected = new OldCoffee(size, 0.0);

            assertEquals(expected, toBeMade);
        }

        @EnumSource(value = Size.class)
        @ParameterizedTest(name = "Showcase for record (hashCode) with a OldCoffee of size {0}")
        void equalHashCode(final Size size) {

            final OldCoffee toBeMade = new OldCoffee(size, 0.0);
            final OldCoffee expected = new OldCoffee(size, 0.0);

            assertEquals(expected.hashCode(), toBeMade.hashCode());
        }

        @EnumSource(value = Size.class)
        @ParameterizedTest(name = "Showcase for record (toString) with a OldCoffee of size {0}")
        void equalToStringValue(final Size size) {

            final OldCoffee toBeMade = new OldCoffee(size, 0.0);
            final OldCoffee expected = new OldCoffee(size, 0.0);

            System.out.println(toBeMade);

            assertEquals(expected.toString(), toBeMade.toString());
        }
    }

    @Test
    void negativePrice() {
        final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> new OldCoffee(Size.LARGE, -1));

        assertEquals("Price should not be negative", exception.getMessage());
    }
}